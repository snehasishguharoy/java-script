package com.example;

import java.io.BufferedReader;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.stream.Collectors;
import java.util.stream.Stream;

public class AbcUtils {

	public static void main(String[] args) throws IOException {
		List<File> filesInFolder = Files.walk(Paths.get("C:\\Users\\sneha\\OneDrive\\Desktop\\target"))
				.filter(Files::isRegularFile).map(Path::toFile).collect(Collectors.toList());
		System.out.println(filesInFolder);
		writeToFirstLine(filesInFolder);
		// writeToFinalLine(filesInFolder);

	}


	private static void writeToFirstLine(List<File> filesInFolder) throws IOException {
		List<String> lines = new CopyOnWriteArrayList<>();
		List<String> output = new ArrayList<>();
		for (File file : filesInFolder) {

			lines = Files.readAllLines(Paths.get(file.getPath()), StandardCharsets.UTF_8);

			lines.add(0, "BEGIN");
			lines.add(lines.size(), "END;");
			lines.stream().forEach(System.out::println);
			FileWriter writer = new FileWriter(file.getPath());
			for (String str : lines) {
				writer.write(str + System.lineSeparator());
			}
			writer.close();
			Path path = Paths.get(file.getAbsolutePath());
			Stream<String> lines1 = Files.lines(path);
			List<String> replaced = lines1.map(line -> line.replaceAll(";", "';")).collect(Collectors.toList());
			lines1.close();
			Files.write(path, replaced);
			Stream<String> lines2 = Files.lines(path);
			List<String> replaced1= lines2.map(line -> line.replaceAll("CREATE TABLE", "IMPLEMENT 'CREATE TABLE")).collect(Collectors.toList());
			Files.write(path, replaced1);
			lines2.close();
			Stream<String> lines3 = Files.lines(path);
			List<String> replaced2= lines3.map(line -> line.replaceAll("ALTER TABLE", "IMPLEMENT 'ALTER TABLE")).collect(Collectors.toList());
			Files.write(path, replaced2);
			lines3.close();
			Stream<String> lines4 = Files.lines(path);
			List<String> replaced3= lines4.map(line -> line.replaceAll("END';", "END;")).collect(Collectors.toList());
			Files.write(path, replaced3);
			lines4.close();
			

		}
	}

}
